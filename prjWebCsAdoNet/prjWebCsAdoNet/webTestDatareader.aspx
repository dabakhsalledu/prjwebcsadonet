﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="webTestDatareader.aspx.cs" Inherits="prjWebCsAdoNet.webTestDatareader" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<script type="text/javascript">

    function ConfirmerEffacer() {
        return window.confirm("Etes de vouloir effacer ce cours ?");
    }

</script>

<style type="text/css">
    #grandtableau{
        width:700px;
        border-radius:5px;
        margin:auto;
        background-color:aqua;
        font-weight:bold;
    }
    #petittableau{
        width:510px;
        border-radius:5px;
        background-color:aquamarine;
        font-weight:bold;
        border:2px solid;
    }
    .boite{
        width:250px;
        font-weight:bold;
        border-radius:5px;
        color:brown;
    }
    .grid{
        text-align:center;
        width:100%;
    }
    .bouton{
        width:150px;
        font-weight:bold;
        border-radius:3px;
        background-color:black;
        color:aquamarine;
    }



</style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1 style="text-align:center">OBJET DATAREADER</h1>
            <hr style="width:370px" />
            <h2 style="text-align:center">Institut TECCART - Gestion Des Cours</h2>
            <hr style="width:400px" />

            <table id="grandtableau">
                <tr>
                    <td>
                        Choisir Cours<br />
                        
                        <asp:ListBox ID="lstCours" AutoPostBack="true" runat="server" ForeColor="Blue" Font-Bold="true" Width="160px" Height="150px" OnSelectedIndexChanged="lstCours_SelectedIndexChanged"></asp:ListBox>
                    </td>
                    <td>

                        <table id="petittableau">
                            <tr>
                                <th colspan="3">Informations du cours</th>
                            </tr>
                            <tr>
                                <td>Numero : </td>
                                <td>
                                    <asp:TextBox ID="txtNumero" runat="server" CssClass="boite"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button ID="btnAjouter" runat="server" Text="Ajouter" CssClass="bouton" OnClick="btnAjouter_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>Titre : </td>
                                <td>
                                    <asp:TextBox ID="txtTitre" runat="server" CssClass="boite"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button ID="btnModifier" runat="server" Text="Modifier" CssClass="bouton" OnClick="btnModifier_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>Professeur : </td>
                                <td>
                                    <asp:TextBox ID="txtProfesseur" runat="server" CssClass="boite"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button ID="btnSupprimer" runat="server" Text="Supprimer" OnClientClick="return ConfirmerEffacer();" CssClass="bouton" OnClick="btnSupprimer_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>Duree : </td>
                                <td>
                                    <asp:TextBox ID="txtDuree" runat="server" CssClass="boite"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button ID="btnSauvegarder" runat="server" Text="Sauvegarder" CssClass="bouton" OnClick="btnSauvegarder_Click" />
                                </td>
                            </tr>
                            <tr>
                                
                                <td colspan="2">
                                    <asp:Label ID="lblInfo" runat="server" Font-Bold="True" ForeColor="Red" Width="100%"></asp:Label>
                                </td>
                                <td>
                                    <asp:Button ID="btnAnnuler" runat="server" Text="Annuler" CssClass="bouton" OnClick="btnAnnuler_Click" />
                                </td>
                            </tr>

                        </table>

                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="gridEtudiants" runat="server" BackColor="Aquamarine" CssClass="grid"></asp:GridView>
                    </td>
                </tr>


            </table>




            
        </div>
    </form>
</body>
</html>
