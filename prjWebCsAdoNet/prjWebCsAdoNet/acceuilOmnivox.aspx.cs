﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prjWebCsAdoNet
{
    public partial class acceuilOmnivox : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Int16 nombreMsg = 0;
            lblMessage.Text = "Bienvenue " + Session["MembreNom"].ToString();
            // connection a la BD et recherche par SQL
            SqlConnection mycon = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=omnivoxDBh23_446;Integrated Security=True");
            mycon.Open();
            Int32 refM = Convert.ToInt32(Session["MembreID"]);
            // requete de recupereation des messages recu par ce membre
            string sql = "SELECT Messages.RefMessage,Messages.Nouveau, Messages.Titre, Messages.Envoyeur, Membres.Nom FROM Messages, Membres WHERE Membres.RefMembre=Messages.Envoyeur AND Messages.Receveur =" + refM ;
            SqlCommand mycmd = new SqlCommand(sql, mycon);
            SqlDataReader myRder = mycmd.ExecuteReader();

            // creer la ligne de titre pour le tableau
            TableRow maligne = new TableRow();
            maligne.BackColor = Color.Black;
            maligne.ForeColor = Color.White;
           
            TableCell maCol = new TableCell();
            maCol.Text = "Titres";
            maCol.Width = 450;
            maligne.Cells.Add(maCol);


            maCol = new TableCell();
            maCol.Text = "Provenances";
            maCol.Width = 225;
            maligne.Cells.Add(maCol);

            maCol = new TableCell();
            maCol.Text = "Actions";
            maCol.Width = 225;
            maligne.Cells.Add(maCol);

            tabMessages.Rows.Add(maligne);


            // boucler a travers les messages recus par ce membre
            while (myRder.Read())
            {
                // creer une ligne (ou row) dans le tableau pour chaque message
                maligne = new TableRow();

                maCol = new TableCell();
                maCol.Text = myRder["Titre"].ToString();
                maligne.Cells.Add(maCol);

                maCol = new TableCell();
                maCol.Text = myRder["Nom"].ToString();
                maligne.Cells.Add(maCol);

                Int32 MsgRef = Convert.ToInt32(myRder["RefMessage"]);
                maCol = new TableCell();
                maCol.Text = "<a href='lireMessageOmnivox.aspx?idMsg=" + MsgRef + "' >Lire</a>&nbsp; &nbsp; &nbsp; ";
                maCol.Text += "<a href='effacerMessageOmnivox.aspx?idMsg=" + MsgRef + "' >Effacer</a> ";
                maligne.Cells.Add(maCol);

                // verifier si message est nouveau et changer la couleur par consequence
                tabMessages.Rows.Add(maligne);

                if (myRder["Nouveau"].ToString() == "True")
                {
                    maligne.BackColor = Color.LightGoldenrodYellow;
                }

                nombreMsg++ ;
            }
            
            myRder.Close();
            mycon.Close();

            lblMessage.Text += "<br />Vous avez " + nombreMsg + " messages";
        }

        protected void btnRediger_Click(object sender, EventArgs e)
        {
            Response.Redirect("ecrireMessageOmnivox.aspx");
        }
    }
}