﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="inscrireOmnivox.aspx.cs" Inherits="prjWebCsAdoNet.inscrireOmnivox" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <style type="text/css">
        body{
            background-color:white;
            font-weight:bold;
        }
        table{
            background-color:aquamarine;
            font-weight:bold;
            color:black;
            border-radius:10px;
            width:400px;
            
            margin:auto;
        }
        h1{
            text-align:center;
        }
        .boite{
            width:200px;
            color:orangered;
            font-weight:bold;
            border-radius:5px;
        }
        .bouton{
            width:150px;
            color:orangered;
            background-color:black;
            font-weight:bold;
            border-radius:5px;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>OMNIVOX-INSCRIPTION MEMBRE</h1>
            <hr style="width:800px" />
            <table>
                <tr>
                    <td>Numero Etudiant :</td>
                    <td>
                        <asp:TextBox ID="txtNumero" CssClass="boite" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="reqNumero" runat="server" Text="*" ForeColor="Red" ControlToValidate="txtNumero" ErrorMessage="Numero Requis !"></asp:RequiredFieldValidator>
                    </td>

                </tr>
                <tr>
                    <td>Prenom Nom :</td>
                    <td>
                        <asp:TextBox ID="txtNom" CssClass="boite" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="reqNom" runat="server" Text="*" ForeColor="Red" ControlToValidate="txtNom" ErrorMessage="Nom et Prenom Requis !"></asp:RequiredFieldValidator>
                    </td>

                </tr>
                <tr>
                    <td>Date Naissance :</td>
                    <td>
                        <asp:TextBox ID="datNaissance" CssClass="boite" runat="server" TextMode="Date"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="reqNaissance" runat="server" Text="*" ForeColor="Red" ControlToValidate="datNaissance" ErrorMessage="Date Naissance Requise !"></asp:RequiredFieldValidator>
                    </td>

                </tr>
                <tr>
                    <td>Email personnel :</td>
                    <td>
                        <asp:TextBox ID="txtEmail" CssClass="boite" runat="server" TextMode="Email"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="reqEmail" runat="server" Text="*" ForeColor="Red" ControlToValidate="txtEmail" ErrorMessage="Email adresse Requise !"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="reqExpEmail" runat="server" ErrorMessage="Format Email Incorrect !" ControlToValidate="txtEmail" Text="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </td>

                </tr>
                <tr>
                    <td>Mot de passe :</td>
                    <td>
                        <asp:TextBox ID="txtMot2Passe" CssClass="boite" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="reqMot2passe" runat="server" Text="*" ForeColor="Red" ControlToValidate="txtMot2Passe" ErrorMessage="Mot De Passe Requis !"></asp:RequiredFieldValidator>
                    </td>

                </tr>
                <tr>
                    <td>Confirmer Mot Passe:</td>
                    <td>
                        <asp:TextBox ID="txtMot2Passe2" CssClass="boite" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                    <td>
                        <asp:CompareValidator ID="cmpMot2Passe" runat="server" ErrorMessage="Mots de passe non identiques !" ForeColor="Red" ControlToCompare="txtMot2Passe" ControlToValidate="txtMot2Passe2" Text="*"></asp:CompareValidator>
                        <asp:RequiredFieldValidator ID="reqMot2passe2" runat="server" Text="*" ForeColor="Red" ControlToValidate="txtMot2Passe2" ErrorMessage="Mot De Passe Requis !"></asp:RequiredFieldValidator>
                    </td>

                </tr>
                
                <tr>
                    <td>&nbsp</td>
                    <td>&nbsp</td>
                    <td>&nbsp</td>

                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnInscrire" runat="server" Text="Inscrire" CssClass="bouton" OnClick="btnInscrire_Click" />
                    </td>
                    <td><asp:Button ID="btnRecommencer" runat="server" Text="Recommencer" CssClass="bouton" /></td>
                    <td>

                    </td>

                </tr>
                
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblErreur" runat="server" Font-Bold="True" ForeColor="Red" Width="100%"></asp:Label>
                    </td>
                    

                </tr>
                <tr>
                    
                    <td colspan="3">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" />
                    </td>
                    

                </tr>

            </table>
        </div>
    </form>
</body>
</html>
