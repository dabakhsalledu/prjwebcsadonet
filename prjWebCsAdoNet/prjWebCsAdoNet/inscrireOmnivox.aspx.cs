﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prjWebCsAdoNet
{
    public partial class inscrireOmnivox : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnInscrire_Click(object sender, EventArgs e)
        {
            // recuperer les valeurs entrees
            string num = txtNumero.Text.Trim();
            Int32 anNais = Convert.ToDateTime(datNaissance.Text).Year;
            string eml = txtEmail.Text.Trim();

            // connection a la BD et recherche par SQL
            SqlConnection mycon = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=omnivoxDBh23_446;Integrated Security=True");
            mycon.Open();

            // creer une requete de recherche
            string sql = "SELECT RefEtudiant, Nom FROM Etudiants ";
            sql += "WHERE Numero ='" + num + "' AND AnneeNaissance =" + anNais;
            sql += " AND Email ='" + eml + "'";
            SqlCommand mycmd = new SqlCommand(sql, mycon);
            SqlDataReader myRder = mycmd.ExecuteReader();
            if (myRder.Read() == true)// si etudiant trouve
            {
                string nom = myRder["Nom"].ToString();
                myRder.Close();
                // verifier si etudiant n'est pas DEJA membre
                sql = "SELECT RefMembre FROM Membres WHERE Numero ='" + num + "'";
                SqlCommand mycmd2 = new SqlCommand(sql, mycon);
                SqlDataReader myRder2 = mycmd2.ExecuteReader();
                if (myRder2.HasRows == true) // si deja membre
                {
                    lblErreur.Text = "Vous etes deja membre, Contacter administration";
                    myRder2.Close();
                }
                else // user est etudiant ET n'est pas encore membre Alors il faut l'ajouter comme membre
                {
                    myRder2.Close();
                    string mdp = txtMot2Passe.Text.Trim();
                    sql = "INSERT INTO Membres(Numero, Nom, Mot2Passe, Statut) ";
                    sql += "VALUES('" + num + "','" + nom + "','" + mdp + "','actif')";
                    SqlCommand mycmd3 = new SqlCommand(sql, mycon);
                    mycmd3.ExecuteNonQuery();
                    // apres inscription, redirriger le user a la page de login
                    mycon.Close();
                    Server.Transfer("indexOmnivox.aspx");
                }
            }
            else // user n'est etudiant
            {   lblErreur.Text = "Ce site est SEULEMENT pour les etudiants";  }
            mycon.Close();
        }
    }
}