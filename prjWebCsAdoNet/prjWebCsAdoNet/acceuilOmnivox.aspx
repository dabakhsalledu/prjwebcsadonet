﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="acceuilOmnivox.aspx.cs" Inherits="prjWebCsAdoNet.acceuilOmnivox" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .tableau{
            width:900px;
            margin:auto;
            border-radius:5px;
            border:2px solid;
            font-weight:bold;
            color:black;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1 style="text-align:center">OMNIVOX-TECCART<br />Boite De Reception De MIO</h1>
            <hr />
            <p style="text-align:center">
            <asp:Label ID="lblMessage" runat="server" Text="Label" Font-Bold="True"></asp:Label>
             </p>
            <asp:Table ID="tabMessages" runat="server" CssClass="tableau" GridLines="Both" ></asp:Table>
            <asp:Button ID="btnRediger" runat="server" Text="Composer un Nouveau message" OnClick="btnRediger_Click" />
        </div>
    </form>
</body>
</html>
