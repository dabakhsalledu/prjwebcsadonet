﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prjWebCsAdoNet
{
    public partial class lireMessageOmnivox : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                Int32 refM  = Convert.ToInt32(Request.QueryString["idMsg"]);
                // connection a la BD et recherche par SQL
                SqlConnection mycon = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=omnivoxDBh23_446;Integrated Security=True");
                mycon.Open();

                // requete de recherche du memebre avec le numero et mot passe entres
                string sql = "SELECT * FROM Messages WHERE RefMessage =" + refM;
                SqlCommand mycmd = new SqlCommand(sql, mycon);
                SqlDataReader myRder = mycmd.ExecuteReader();
                if (myRder.Read())
                {
                    lblTitre.Text = myRder["Titre"].ToString();
                    lblDate.Text = myRder["DateCreation"].ToString();
                    lblMessage.Text = myRder["Message"].ToString();

                }
                myRder.Close();

                // requete pour mettre a jour le champ nouveau, message lu
                sql = "UPDATE Messages SET Nouveau='False' WHERE RefMessage =" + refM;
                SqlCommand cmd = new SqlCommand(sql, mycon);
                cmd.ExecuteNonQuery();

                mycon.Close();
            }
             
        }
    }
}