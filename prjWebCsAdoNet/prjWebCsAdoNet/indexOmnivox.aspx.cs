﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient; // sql server

namespace prjWebCsAdoNet
{
    public partial class indexOmnivox : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            // Recuperer numero et mot de passe
            string numEtud = txtNumero.Text.Trim();
            string motpasse = txtMot2passe.Text.Trim();

            // connection a la BD et recherche par SQL
            SqlConnection mycon = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=omnivoxDBh23_446;Integrated Security=True");
            mycon.Open();

            // requete de recherche du memebre avec le numero et mot passe entres
            string sql = "SELECT RefMembre, Nom FROM Membres WHERE Numero = '" + numEtud +
                "' AND Mot2Passe = '" + motpasse + "'";
            SqlCommand mycmd = new SqlCommand(sql, mycon);
            SqlDataReader myRder = mycmd.ExecuteReader();

            // verifier si membre a ete trouve
            if (myRder.Read() == true)
            {
                // sauvegarder refmembre et nom dans des variables globales (de session)
                Session["MembreID"] = myRder["RefMembre"];
                Session["MembreNom"] = myRder["Nom"];

                myRder.Close();
                mycon.Close();
                // redirriger vers la page d'acceuil
                Server.Transfer("acceuilOmnivox.aspx");
            }
            else // si myrder est vide ou membre inexistant
            {
                myRder.Close();
                mycon.Close();
                lblErreur.Text = "Numero ou Mot de passe incorrect, Essayez de nouveau";
            }
                



        }

        protected void btnInscrire_Click(object sender, EventArgs e)
        {
            Response.Redirect("inscrireOmnivox.aspx");
        }
    }
}