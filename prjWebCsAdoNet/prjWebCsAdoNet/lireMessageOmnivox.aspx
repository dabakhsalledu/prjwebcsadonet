﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="lireMessageOmnivox.aspx.cs" Inherits="prjWebCsAdoNet.lireMessageOmnivox" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style >
        table{
            width:500px;
            font-weight:bold;
            margin:auto;
            border-radius:5px;
            border:2px solid;
            background-color:antiquewhite;
            color:brown;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1 style="text-align:center">OMNIVOX-TECCART<br />Lecture De Message</h1>
            <hr />
            <table>
                <tr>
                    <td>Titre :</td>
                    <td>
                        <asp:Label ID="lblTitre" runat="server" Text="Label"></asp:Label>
                    </td>
                 </tr>
                <tr>
                    <td>Date :</td>
                    <td>
                        <asp:Label ID="lblDate" runat="server" Text="Label"></asp:Label>
                    </td>
                 </tr>
                <tr>
                    <td>Message :</td>
                    <td>
                        <asp:Label ID="lblMessage" runat="server" Text="Label"></asp:Label>
                    </td>
                 </tr>


            </table>


        </div>
    </form>
</body>
</html>
