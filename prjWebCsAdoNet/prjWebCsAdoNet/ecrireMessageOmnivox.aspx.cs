﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prjWebCsAdoNet
{
    public partial class ecrireMessageOmnivox : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                // connection a la BD 
                SqlConnection mycon = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=omnivoxDBh23_446;Integrated Security=True");
                mycon.Open();

                // requete d
                string sql = "SELECT RefMembre, Nom, Numero FROM Membres ORDER BY Nom" ;
                SqlCommand mycmd = new SqlCommand(sql, mycon);
                SqlDataReader myRder = mycmd.ExecuteReader();
                while(myRder.Read())
                {
                    ListItem elm = new ListItem();
                    elm.Text = myRder["Nom"].ToString() + " ( " + myRder["Numero"].ToString() + " )";
                    elm.Value = myRder["RefMembre"].ToString();
                    cboDestinaires.Items.Add(elm);
                }
                myRder.Close();
                mycon.Close();
               
            }
        }
                protected void btnEnvoyer_Click(object sender, EventArgs e)
        {
            // recuperer les valeurs entrees
            string tit = txtTitre.Text.Trim();
            string msg = txtMessage.Text.Trim();
            Int32 refEnvoyeur = Convert.ToInt32(Session["MembreID"]);
            Int32 refReceveur = Convert.ToInt32(cboDestinaires.SelectedItem.Value);
            // connection a la BD 
            SqlConnection mycon = new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=omnivoxDBh23_446;Integrated Security=True");
            mycon.Open();

            // requete d
            string sql = "INSERT INTO Messages (Titre, Message, Envoyeur, Receveur, DateCreation, Nouveau) " +
                "VALUES('" + tit + "','" + msg + "'," + refEnvoyeur + "," + refReceveur + ",'" + DateTime.Now.ToShortDateString() + "','True')";
            SqlCommand mycmd = new SqlCommand(sql, mycon);
            mycmd.ExecuteNonQuery();
            mycon.Close();
            Response.Redirect("acceuilOmnivox.aspx");
        }
    }
}