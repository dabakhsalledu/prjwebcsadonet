﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;

namespace prjWebCsAdoNet
{
    public partial class webTestDatareader : System.Web.UI.Page
    {
        // declaration variables globales a la page
        static OleDbConnection mycon;
        static string mode;
        static Int32 refCour;
        static Int32 indiceCourChoisi;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                // etape 1 Creation connection
                mycon = new OleDbConnection();
                mycon.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Server.MapPath("~\\App_Data\\CollegeDB.accdb;Persist Security Info=True");
                

                RemplirListeCours();

               

                // selectionner le premier cours par defaut
                lstCours.SelectedIndex = 0;
                lstCours_SelectedIndexChanged(sender, e);

                MontrerBoutons(true, false);
            }
            
            
        }

        private void RemplirListeCours()
        {
            // etape 1 ouvrir connection
            mycon.Open();
            // etape 2 Creation command
            string sql = "SELECT Numero FROM Cours";
            OleDbCommand mycmd = new OleDbCommand();
            mycmd.CommandText = sql;
            mycmd.Connection = mycon;

            // etape 3 Creation datareader
            OleDbDataReader myrder = mycmd.ExecuteReader();

            // Remplir le listbox avec le datareader
            //// version boucle
            //while (myrder.Read() == true)
            //{
            //    ListItem elm = new ListItem();
            //    elm.Text = myrder["Numero"].ToString();
            //    // elm.Value = ?
            //    lstCours.Items.Add(elm);
            //}
            //myrder.Close();

            // Version Databinding
            lstCours.DataTextField = "Numero";
            lstCours.DataSource = myrder;
            lstCours.DataBind();

            mycon.Close();
        }

        private void MontrerBoutons(bool AjModSup, bool SauvAnn)
        {
            btnAjouter.Visible = btnModifier.Visible = btnSupprimer.Visible = AjModSup;
            btnSauvegarder.Visible = btnAnnuler.Visible = SauvAnn;
        }

        protected void lstCours_SelectedIndexChanged(object sender, EventArgs e)
        {
            // recuperer le numero selectionne dans le listbox
            string numerochoisi = lstCours.SelectedItem.Text;

            indiceCourChoisi = lstCours.SelectedIndex;
            refCour = 0;
            // ouverture de la connection
            mycon.Open();

            // Creation command
            string sql = "SELECT RefCours, Numero, Titre, Professeur, Duree FROM Cours WHERE Numero ='" + numerochoisi + "'";
            OleDbCommand mycmd = new OleDbCommand();
            mycmd.CommandText = sql;
            mycmd.Connection = mycon;

            OleDbDataReader myRder = mycmd.ExecuteReader();
            if (myRder.Read())
            {
                txtNumero.Text = myRder["Numero"].ToString();
                txtTitre.Text = myRder["Titre"].ToString();
                txtProfesseur.Text = myRder["Professeur"].ToString();
                txtDuree.Text = myRder["Duree"].ToString();
                refCour = Convert.ToInt32(myRder["RefCours"]);
            }
            myRder.Close();

            // aller chercher tous les etudiants de ce cours
            sql = "SELECT Numero,Nom,Naissance,Email,Moyenne FROM Etudiants ";
            sql += "WHERE ReferCours =" + refCour;
            OleDbCommand mycmd2 = new OleDbCommand();
            mycmd2.CommandText = sql;
            mycmd2.Connection = mycon;

            OleDbDataReader rderEtuds = mycmd2.ExecuteReader();
            // databinding gridview avec datareader
            gridEtudiants.DataSource = rderEtuds;
            gridEtudiants.DataBind();


            mycon.Close();
        }

        protected void btnAjouter_Click(object sender, EventArgs e)
        {
            mode = "ajout";
            txtDuree.Text = txtNumero.Text = txtProfesseur.Text = txtTitre.Text = "";
            txtNumero.Focus();
            lblInfo.Text = "En Mode Ajout";
            MontrerBoutons(false, true);
        }

        protected void btnModifier_Click(object sender, EventArgs e)
        {
            mode = "modif";
            txtNumero.Focus();
            lblInfo.Text = "En Mode Modification";
            MontrerBoutons(false, true);
        }

        protected void btnSauvegarder_Click(object sender, EventArgs e)
        {
            // recuperation des valeurs des textbox
            string num = txtNumero.Text.Trim();
            string tit = txtTitre.Text.Trim();
            string prof = txtProfesseur.Text.Trim();
            Int32 dur = Convert.ToInt32(txtDuree.Text);

            string sql = "";

            Int32 indexcourant = 0;

            if (mode == "ajout")
            {

                sql = "INSERT INTO Cours(Numero, Titre, Duree, Professeur) ";
                sql += "VALUES('" + num + "','" + tit + "'," + dur + ",'" + prof + "')";

                // apres pour afficher le dernier cours ajoute on trouve son indice
                indexcourant = lstCours.Items.Count - 1;
            }
            else if(mode == "modif")
            {
                sql = "UPDATE Cours SET Numero ='" + num + "', Titre ='" + tit +
                    "', Duree =" + dur + ", Professeur ='" + prof + "' " + 
                    "WHERE RefCours =" + refCour;
                // apres pour afficher le cours choisi, on trouve son indice
                indexcourant = indiceCourChoisi ;
            }
            mycon.Open();
            OleDbCommand mycmd = new OleDbCommand(sql,mycon);
            //mycmd.CommandText = sql;
            //mycmd.Connection = mycon;
            mycmd.ExecuteNonQuery();
            mycon.Close();

           
            lblInfo.Text = "";
            MontrerBoutons(true, false);
            RemplirListeCours();
            lstCours.SelectedIndex = indexcourant;
            lstCours_SelectedIndexChanged(sender, e);

            mode = "";
        }

        protected void btnAnnuler_Click(object sender, EventArgs e)
        {
            lstCours.SelectedIndex = indiceCourChoisi;
            lstCours_SelectedIndexChanged(sender, e);
            MontrerBoutons(true, false);
        }

        protected void btnSupprimer_Click(object sender, EventArgs e)
        {
            try
            {
            string sql = "DELETE FROM Cours WHERE RefCours =" + refCour;
            mycon.Open();
            OleDbCommand mycmd = new OleDbCommand(sql, mycon);
            mycmd.ExecuteNonQuery();
            mycon.Close();

            RemplirListeCours();
            lstCours.SelectedIndex = 0;
            lstCours_SelectedIndexChanged(sender, e);
            }
            catch(Exception ex)
            {
                // lblInfo.Text = ex.Message;
                
                lblInfo.Text = "Impossible de supprimer un cours qui contient des etudiants";
            }

        }
    }
}