﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ecrireMessageOmnivox.aspx.cs" Inherits="prjWebCsAdoNet.ecrireMessageOmnivox" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

<style >
        table{
            width:500px;
            font-weight:bold;
            margin:auto;
            border-radius:5px;
            border:2px solid;
            background-color:antiquewhite;
            color:brown;
        }
        .boite{
            width:200px;
            font-weight:bold;
            border-radius:2px;
            border:2px solid;
        }
        .bouton{
            width:150px;
            font-weight:bold;
            border-radius:2px;
            border:2px solid;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1 style="text-align:center">OMNIVOX-TECCART<br />Lecture De Message</h1>
            <hr />
            <table>
                <tr>
                    <td>Destinataire :   </td>
                    <td>
                        <asp:DropDownList ID="cboDestinaires" runat="server" CssClass="boite" Width="337px"></asp:DropDownList>
                    </td>
                </tr>

                <tr>
                    <td>Titre :   </td>
                    <td>
                        <asp:TextBox ID="txtTitre" runat="server" CssClass="boite" Width="330px"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">Message :   </td>
                    
                </tr>

                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtMessage" runat="server" CssClass="boite" Height="200px"
                            TextMode="MultiLine" Width="490px"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Button ID="btnEnvoyer" runat="server" Text="Envoyer" CssClass="bouton" OnClick="btnEnvoyer_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnAnnuler" runat="server" Text="Annuler" CssClass="bouton" />
                    </td>
                </tr>


            </table>
        </div>
    </form>
</body>
</html>
